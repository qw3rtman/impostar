var express = require('express');
var app = express();

// Set template engine to Handlebars.js.
app.set('view options', { layout: 'hbs' });

// Routes includes.
var routes = require('./routes/index');
var github = require('./routes/github');

var server = app.listen(3000, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('imposter listening at http://%s:%s', host, port); // imposter -> impostar.
});

app.use('/', routes);
app.use('/github', github);

/* Nimit, the name is Impostar, not Imposter. :P
                            ^             ^
*/
