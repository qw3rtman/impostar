var express = require('express');
var router = express.Router();

/* GET home page. */
// respond with "Hello World!" on the homepage
router.get('/', function (req, res) {
  res.send('Hello World!'); // ERROR res.send(status) is deprecated. Use res.sendStatus(status) instead. (Error only occurs when parameter is not a string.)
});

module.exports = router;
