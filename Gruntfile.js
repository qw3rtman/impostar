module.exports = function (grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		sass: {
			dist: {
				options: {
					style: 'compressed',
				},

				files: {
					'public/css/style.css': 'public/sass/style.scss'
				}
			},
		},

		watch: {
			options: {
				spawn: false,
				livereload: 1337,
			},

			/* Compilation */
			compileJs: {
				files: ['public/js/libs/*.js', 'public/js/src/*.js'],
				tasks: ['uglify'],
				options: {
					livereload: true
				}
			},

			compileSass: {
				files: ['public/sass/*.scss'],
				tasks: ['sass:dist'],
				options: {
				  livereload: true
				}
			},

			/* Live Reload */
//			html: {
//				files: ['**/*.html'],
//				tasks: [],
//				options: {
//				  livereload: true
//				}
//			},
//
//			css: {
//				files: ['public/*.css'],
//				tasks: [],
//				options: {
//				  livereload: true
//				}
//			},
//
//			js: {
//				files: ['public/js/*.js'],
//				tasks: [],
//				options: {
//					livereload: true
//				}
//			},

			expressNoBackground: {
				files: ['**/*.js','*.js','!public/*.js','!public/**/*.js'],
				tasks: ['express:nobg'],
				options: {
					spawn: false
				}
			},

			expressDev: {
				files: ['**/*.js','*.js','!public/*.js','!public/**/*.js'],
				tasks: ['express:dev'],
				options: {
					spawn: false
				}
			}
		},

		uglify: {
			options: {
				sourceMap: true,
				sourceMapName: 'public/js/global.source.map'
			},

			build: {
				src: ['public/js/src/*.js'],
				dest: 'public/js/global.js'
			},
		},

		connect: {
			server: {
				options: {
					port: '8000',
					base: 'public'
				},
			},
		},

		autoprefixer: {
			options: {
				browsers: ['last 2 versions', 'ie 8', 'ie 9', 'ie 10', 'opera', 'firefox']
			},
			prefix: {
				src: 'public/css/*.css',
			},
		},

		express: {
			options: {
				cmd: process.argv[0],
				opts: [''],
				args: [''],
				background: false, // Important
				fallback: function () {},
				port: 3000,
				node_env: undefined,
				delay: 0,
				output: ".+",
				debug: false,
				breakOnFirstLine: false,
				logs: undefined
			},

			nobg: {
				options: {
					script: "app.js",
					background: false
				}
			},

			dev: {
				options: {
					script: "app.js",
					background: true
				}
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-express-server');

	grunt.registerTask('default', [
		'sass',
		'autoprefixer',
		'uglify',
		'connect',
		'watch'
	]);

	grunt.registerTask('compile', [
		'uglify',
		'sass',
		'autoprefixer'
	]);

	grunt.registerTask('server', [
		'uglify',
		'autoprefixer',
		'connect'
	]);

	grunt.registerTask('nimit', [
		'connect',
		'express:nobg'
	]);

	grunt.registerTask('didier', [
		'connect',
		'express:dev',
		'watch:expressDev'
	]);
};
